package com.opensoftware.helptec.controller;

import com.opensoftware.helptec.domain.Goal;
import com.opensoftware.helptec.repository.GoalRepository;
import com.opensoftware.helptec.ui.GoalCell;
import java.io.IOException;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;

public class ShowGoalsController {
	
	private GoalRepository goalRepository = new GoalRepository();
	
	ObservableList<Goal> observableList = FXCollections.observableArrayList();
	private Set<Goal> stringSet;
	
	@FXML
	private ListView<Goal> listView;
	
	public ShowGoalsController() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/showGoals.fxml"));
		try {
			Parent parent = fxmlLoader.load();
			Scene scene = new Scene(parent);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void setListView() {
		stringSet.addAll(goalRepository.findAll());
		
		observableList.setAll(stringSet);
		listView.setItems(observableList);
		listView.setCellFactory(listView -> new GoalCell());
	}
	
	@FXML
	protected void initialize() {
		setListView();
	}
}
