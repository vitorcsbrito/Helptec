package com.opensoftware.helptec.controller;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class CustomCellController {
	public BorderPane billItem;
	public Label numberOfItems;
	public Label itemName;
	public Label price;
}
