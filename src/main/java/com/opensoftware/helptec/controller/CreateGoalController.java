package com.opensoftware.helptec.controller;

import com.opensoftware.helptec.domain.Goal;
import com.opensoftware.helptec.repository.GoalRepository;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class CreateGoalController {
	public DatePicker startDateDatePicker;
	public DatePicker endDateDatePicker;
	public TextField descriptionField;
	public TextArea contextTextArea;
	public Button saveButton;
	public Button cancelButton;
	public TextArea motivationTextArea;
	
	private GoalRepository goalRepository = new GoalRepository();
	
	public void saveButtonOnClick(ActionEvent actionEvent) {
		if (startDateDatePicker.getValue() != null && endDateDatePicker.getValue() != null && descriptionField.getText() != null) {
			LocalDate localDate = startDateDatePicker.getValue();
			Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Date start = Date.from(instant);
			
			localDate = endDateDatePicker.getValue();
			instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Date end = Date.from(instant);
			
			goalRepository.save(
					Goal.builder()
							.description(descriptionField.getText())
							.start(start)
							.end(end)
							.context(contextTextArea.getText())
							.motivation(motivationTextArea.getText())
							.notes(new ArrayList<>())
							.build()
			);
			close();
			
		} else {
			Alert alert = new Alert(AlertType.WARNING, "Some information is missing, do a double check and comeback.", ButtonType.OK);
			alert.setResizable(true);
			alert.showAndWait();
		}
		
		
	}
	
	public void cancelButtonOnClick(ActionEvent actionEvent) {
		close();
	}
	
	private void close() {
		Stage stage = (Stage) cancelButton.getScene().getWindow();
		stage.close();
	}
}
