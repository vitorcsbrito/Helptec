package com.opensoftware.helptec.controller;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class CustomGoalButtonController {
	public AnchorPane customButtonPane;
	public Button markDoneGoalButton;
	public Button deleteGoalButton;
	public Label startDateLabel;
	public Label endDateLabel;
	public Label timeLeftLabel;
	
	public void showGoalFullDetails(MouseEvent mouseEvent) {
	}
}
