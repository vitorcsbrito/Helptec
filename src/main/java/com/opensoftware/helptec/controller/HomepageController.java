package com.opensoftware.helptec.controller;

import com.opensoftware.helptec.repository.GoalRepository;
import com.opensoftware.helptec.ui.CreateGoalApplication;
import com.opensoftware.helptec.ui.ShowGoalsApplication;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

public class HomepageController {
	public Button seeGoalsButton;
	public Button createGoalButton;
	
	private GoalRepository goalRepository = new GoalRepository();
	
	public void showGoalsOnClick(ActionEvent actionEvent) {
//		List<Goal> goals = goalRepository.findAll();
//		for (Goal goal : goals) {
//			System.out.println(goal);
//		}
		new ShowGoalsApplication();
	}
	
	public void createGoalOnClick(ActionEvent actionEvent) {
		new CreateGoalApplication();
	}
}
