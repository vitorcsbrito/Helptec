package com.opensoftware.helptec.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import lombok.*;
import lombok.Builder.Default;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Goal implements Serializable {
	private static final long serialVersionUID = -332865924647096277L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected int id;
	
	
	@NonNull
	private String description;
	private String context;
	private String motivation;
	
	@NonNull
	@Temporal(TemporalType.DATE)
	private Date start;
	
	@NonNull
	@Temporal(TemporalType.DATE)
	private Date end;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@Default
	private List<String> notes = new ArrayList<>();
	
}
