package com.opensoftware.helptec.domain;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoalList {
	@Default
	private List<Goal> list = new ArrayList<>();
	
	public boolean addGoal(Goal goal) {
		if (goal != null) {
			list.add(goal);
			return true;
		}
		return false;
	}
}
