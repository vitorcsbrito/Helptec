package com.opensoftware.helptec.ui;

import java.io.IOException;
import java.util.Objects;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CreateGoalApplication extends Application {
	
	public CreateGoalApplication() {
		start(new Stage());
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) {
		Parent root = null;
		try {
			root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/createGoal.fxml")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		stage.setScene(new Scene(Objects.requireNonNull(root)));
		stage.setResizable(false);
		stage.showAndWait();
	}
	
}
