package com.opensoftware.helptec.ui;

import java.io.IOException;
import java.util.Objects;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class HomepageApplication extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) {
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/homepage.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		primaryStage.setScene(new Scene(Objects.requireNonNull(root)));
		primaryStage.setResizable(false);
		primaryStage.show();
	}
}
