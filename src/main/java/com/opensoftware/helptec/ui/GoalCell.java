package com.opensoftware.helptec.ui;

import com.opensoftware.helptec.domain.Goal;
import javafx.scene.control.ListCell;

public class GoalCell extends ListCell<Goal> {
	@Override
	protected void updateItem(Goal goal, boolean b) {
		super.updateItem(goal, b);
		if (goal != null) {
			GoalData data = new GoalData(goal);
			setGraphic(data.getBox());
		}
	}
}
