package com.opensoftware.helptec.ui;

import com.opensoftware.helptec.domain.Goal;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class GoalData {
	@FXML
	public HBox customButtonPane;
	@FXML
	public Button markDoneGoalButton;
	@FXML
	public Button deleteGoalButton;
	@FXML
	public Label goalDescription;
	@FXML
	public Label startDateLabel;
	@FXML
	public Label endDateLabel;
	@FXML
	public Label timeLeftLabel;
	
	public GoalData(Goal goal) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/customGoalButton.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		setInfo(goal);
	}
	
	public void setInfo(Goal data) {
		startDateLabel.setText(data.getStart().toString());
		endDateLabel.setText(data.getEnd().toString());
		
		goalDescription.setText(data.getDescription());
	}
	
	public HBox getBox() {
		return customButtonPane;
	}
}
