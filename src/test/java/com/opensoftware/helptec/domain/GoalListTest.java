package com.opensoftware.helptec.domain;

import java.util.Date;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GoalListTest {
	
	@Test
	public void goalAddToListTest() {
		Goal goal = Goal.builder().description("test")
				            .start(new Date(2019, 2, 14))
				            .end(new Date(2019, 2, 16))
				            .context("unit testing")
				            .build();
		
		GoalList gl = new GoalList();
		boolean b = gl.addGoal(goal);
		assertTrue(b);
		
		b = gl.addGoal(null);
		assertFalse(b);
	}
}
