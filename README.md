# **Helptec**

#### Status Project
[![pipeline status](https://gitlab.com/vitorcsbrito/Helptec/badges/master/pipeline.svg)](https://gitlab.com/vitorcsbrito/Helptec/commits/master)
[![coverage report](https://gitlab.com/vitorcsbrito/Helptec/badges/master/coverage.svg)](https://gitlab.com/vitorcsbrito/Helptec/commits/master)

- **Development**
#### About project
Project consist software where help manager repair devices, in shop repair or repair centers. All feature will be implemented in future.

#### Contribute
- Create issues to find bugs, new ideas or new features;

#### Academic use
- Can use this project for developer work for academic envoirement.

#### Technologies
- Java;
- H2 Databse
- JPA;
- Lombok

#### Patterns
- Factory;
- Repository;
- Package;
- Single responsability;
